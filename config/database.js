const mongoose = require('mongoose');

db_Option = {
    useUnifiedTopology: true,
    useNewUrlParser: true
}
mongoose
    .connect(process.env.database, db_Option)
    .then(() => console.log("database잘 접속됨"))
    .catch(err => console.log(err.message));