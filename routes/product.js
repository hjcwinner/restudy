const express = require('express')
const router = express.Router()

const checkAuth = require('../middleware/checkAuth')
const { products_get_all, products_get_detail, products_update_product, products_post_product, products_delete_product } = require('../controllers/product')


router.get("/", products_get_all)

//////한개불러오기
router.get("/:productid", checkAuth, products_get_detail)

/////등록하기
router.post("/", products_post_product);

/////수정하기
router.patch("/:productid", checkAuth, products_update_product);

/////삭제하기
router.delete("/:prodelete", checkAuth, products_delete_product);

module.exports = router;