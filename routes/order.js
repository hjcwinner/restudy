const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/checkAuth')
const { orders_get_all, orders_get_detail, orders_patch_order, orders_post_order, orders_delete_order } = require('../controllers/order');


////불러오기(get)
router.get("/", checkAuth, orders_get_all)

////1개불러오기
router.get('/:orderid', checkAuth, orders_get_detail)

////등록하기(post)
router.post("/", orders_post_order)

////수정하기(patch)
router.patch("/:orderid", checkAuth, orders_patch_order)

////삭제하기(delete)
router.delete("/:orderid", checkAuth, orders_delete_order)

module.exports = router;
