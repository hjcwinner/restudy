const mongoose = require('mongoose')
const userModel = mongoose.Schema({
    userid : {
        type : String,
        required : true
    },
    password : {
        type : String,
        required : true
    },
    email : {
        type : String,
        required : true
    }
})

module.exports = mongoose.model('user', userModel)
