const productModel = require('../model/product')

exports.products_get_all = (req, res) => {
    productModel
        .find()
        .then(docs => {
            res.json({
                count : docs.length,
                products : docs.map(doc => {
                    return{
                        id : doc._id,
                        name : doc.name,
                        price : doc.price,
                        request : {
                            type : "get",
                            url : "http://localhost:7070/product/" + doc._id
                        }
                    }
                })
            })    
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.products_get_detail = (req, res) => {
    const id = req.params.productid
    productModel
        .findById()
        .then(doc => {
            if(!doc)
            {
                res.json({
                    message : "그런상품없음"
                })
            }
            else
            {
                res.json({
                    message : "product one open  " +  id,
                    productInfo : {
                        id : doc._id,
                        name : doc.name,
                        price : doc.price,
                        request : {
                            type : "get",
                            url : "http://localhost:7070/product/"
                        }
                    }
                })
            }
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.products_post_product = (req, res) => {
    const {name, price} = req.body
    const product = new productModel({
        name, price
    })
    product
        .save()
        .then(doc => {
            res.json({
                message : "prduct save complete",    ////제발 , 까먹지마라
                productIno : {
                    id : doc._id,
                    name : doc.name,
                    price : doc.price,
                    request : {
                        type : "get",
                        url : "http://localhost:7070/product/"
                    }
                }
            })
        })
        .catch(err => {
            res.json({
                error : err.message
            })
        })
}

exports.products_update_product = (req, res) => {
    const updateOps = {}
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value
    }

    productModel
        .findByIdAndUpdate(req.params.productid, { $set : updateOps})
        .then(result => {
            res.json({
                message : "update product",
                productInfo : {
                    id : result._id,
                    name : result.name,
                    price : result.price,
                    request : {
                        type : "get",
                        url : "http://localhost:7070/product/" + result._id
                    }
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.products_delete_product = (req, res) => {
    productModel
        .findByIdAndDelete(req.params.prodelete)
        .then(() => {
            res.json({
                message : "delete complete",
                request : {
                    type : "get",
                    url : "http://localhost:7070/product/"
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}