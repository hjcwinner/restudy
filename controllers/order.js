const orderModel = require('../model/order');

exports.orders_get_all = (req, res) => {
    orderModel
        .find()
        .populate('product',["name","price"])
        .then(aaaa => {
            const response = {
                count : aaaa.length,
                products : aaaa.map(bbb => {
                return{
                    id : bbb._id,
                    product : bbb.product,
                    quantity : bbb.quantity,
                    request : {
                     type : "get",
                     url : "http://localhost:7070/order/" + bbb._id
                    }
                }
            })
        }
        res.json(response)
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.orders_get_detail = (req, res) => {
    const id = req.params.orderid
    orderModel
        .findById(id)
        .populate('product',["name","price"])
        .then(doc => {
            res.json({
                message : "one order get",
                orderInfo : {
                    id : doc._id,
                    product : doc.product,
                    quantity : doc.quantity,
                    request : {
                        type : "get",
                        url : "http://localhost:7070/order"
                    }
                }
            })
        })
}

exports.orders_post_order = (req, res) => {
    const {product, quantity} = req.body
    const order = new orderModel({
        product, quantity
    })

    order
        .save()
        .then(doc => { 
            res.json({
                message : "order save",
                orderInfo : {
                    id : doc._id,
                    product : doc.product,
                    quantity : doc.quantity,
                    request : {
                        type : "get",
                        url : "http://localhost:7070/order"
                    }
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })

}

exports.orders_patch_order = (req, res) => {
    const id = req.params.orderid
    const updateOps = {}
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value
        console.log(updateOps)
    }

    orderModel
        .findByIdAndUpdate(id, { $set : updateOps})
        .then(result => {
            res.json({
                message : "update",
                result : {
                    request : {
                        type : "get",
                        url : "http://localhost:7070/order"
                    }
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.orders_delete_order = (req, res) => {
    const id = req.params.orderid
    orderModel
        .findByIdAndDelete(id)
        .then(() => {
            res.json({
                message : "order delete",
                result : {
                    type : "get",
                    url : "http://localhost:7070/order"
                }
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
     })
}