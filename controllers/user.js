const userModel = require('../model/user')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

exports.user_register = (req, res) => {
    ////email중복체크 ==> password암호화 ==> 회원가입
    userModel
        .findOne({email : req.body.email})
        .then(user => {
            if(user) 
            {
                return res.json({
                    message : "이미 가입된 email"
                })
            }
            else 
            {
                bcrypt.hash(req.body.password, 10 , (err, hash) => {
                    if(err) {
                        return res.json({
                            message : err.message
                        })
                    }
                    else {
                        const newUser = new userModel({
                            userid : req.body.userid,
                            email : req.body.email,
                            password : hash
                        })

                        newUser
                            .save()
                            .then(user =>{
                                res.json({
                                    userInfo : {
                                        id : user._id,
                                        userid : user.userid,
                                        email : user.email,
                                        password : user.password
                                    }
                                })
                            }) 
                            .catch(err => {
                                res.json({
                                    message : err.message
                                })
                            })
                        }
                })
            }
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.user_login = (req, res) => {
    ////email확인 => password확인 => 로그인
    userModel
        .findOne({email : req.body.email})
        .then(user => {
            if(!user) 
            {
                return res.json({
                    message : "가입된 email이 없습니다"
                })
            }
            else
            {
                bcrypt.compare(req.body.password, user.password, (err, isMatch) => {
                    if(err || isMatch === false) {
                        return res.json({
                            message : "password incorrect"
                        })
                    }
                    else {
                        const token = jwt.sign(
                            {userid : user._id},
                            process.env.SECRET_KEY,
                            { expiresIn : 60 }
                        )
                        res.json({
                            message : "successful login",
                            token : token
                        })
                    }
                })
            }
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}

exports.user_get = (req, res) => {
    userModel
        .find()
        .then(docs => {
            res.json({
                count : docs.length,
                userInfo : docs.map(doc => {
                    return{
                        id : doc._id,
                        userid : doc.userid,
                        email : doc.email,
                        password : doc.password
                    }
                })
            })
    
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
}
