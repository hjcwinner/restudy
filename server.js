const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const dotEnv = require('dotenv')
dotEnv.config()


const app = express();

const productRoutes = require('./routes/product');
const orderRoutes = require('./routes/order');
const userRoutes = require('./routes/user')
/////database
require('./config/database')



/////middleware
app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));


app.use('/product', productRoutes);
app.use('/order', orderRoutes);
app.use('/user', userRoutes);

const port = process.env.port;
app.listen(port, console.log("서버시작됨"));



